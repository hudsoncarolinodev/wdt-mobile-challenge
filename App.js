import { StatusBar } from 'expo-status-bar';
import Routes from './src/routes';

import { Provider } from 'react-redux'
import store from './src/store'

export default function App() {
  return (
    <Provider store={store}>
      <StatusBar/>
      <Routes/>
    </Provider>
  );
}
