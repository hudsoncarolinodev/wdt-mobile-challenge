import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import RestaurantsList from './screens/Restaurants/RestaurantsList';
import RestaurantsDetail from './screens/Restaurants/RestaurantsDetail';


export default function Routes() {

    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen 
                    name="Restaurantes" 
                    component={RestaurantsList} 
                />
                 <Stack.Screen 
                    name="Detalhes do restaurante" 
                    component={RestaurantsDetail} 
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
    
}

const Stack = createNativeStackNavigator();

