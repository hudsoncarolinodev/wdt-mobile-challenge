export const GET_RESTAURANTS = 'GET_RESTAURANTS';
export const LOAD_RESTAURANTS = 'LOAD_RESTAURANTS';
export const GET_RESTAURANTS_SUCCESS = 'GET_RESTAURANTS_SUCCESS';
export const GET_RESTAURANT_SUCCESS = 'GET_RESTAURANT_SUCCESS';
export const GET_RESTAURANT = 'GET_RESTAURANT';
export const IS_LOADING = 'IS_LOADING';

export const getRestaurants = () => ({
  type: GET_RESTAURANTS,
});

export const getRestaurantsSuccess = (restaurants) => ({
  type: GET_RESTAURANTS_SUCCESS,
  payload: restaurants,
});

export const getRestaurant = (itemId) => ({
  type: GET_RESTAURANT,
  id: itemId,
});

export const loadPageRestaurant = (restaurants) => ({
  type: LOAD_RESTAURANTS,
  payload: restaurants,
});

export const getRestaurantSuccess = (restaurant) => ({
  type: GET_RESTAURANT_SUCCESS,
  payload: restaurant,
});

export const isLoading = (value) => ({
  type: IS_LOADING,
  payload: value,
});