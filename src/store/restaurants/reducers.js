import {
  GET_RESTAURANTS,
  GET_RESTAURANT,
  GET_RESTAURANTS_SUCCESS,
  GET_RESTAURANT_SUCCESS,
  IS_LOADING
} from './action';

const initialState = {
  restaurants: [],
  restaurant: [],
  totalDocs: 0,
  offset: 0,
  limit: 0,
  page: 0,
  pagingCounter: 0,
  loading: false
};

const restaurantsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_RESTAURANTS:
      return {
        ...state
      };
    case GET_RESTAURANTS_SUCCESS:
      return {
        ...state,
        restaurants: [...state.restaurants, ...action.payload.docs],
        totalDocs: action.payload.totalDocs,
        offset: action.payload.offset + 1,
        limit: action.payload.limit,
        page: action.payload.page,
        pagingCounter: action.payload.pagingCounter
      };
    case GET_RESTAURANT_SUCCESS:
      return {
        ...state,
        restaurant: action.payload
      };
    case IS_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
};

export default restaurantsReducer;