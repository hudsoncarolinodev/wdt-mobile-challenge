import { call, put, select, takeLatest } from 'redux-saga/effects';
import { api } from '../../services/api';
import {
  GET_RESTAURANTS,
  GET_RESTAURANT,
  LOAD_RESTAURANTS,
  isLoading,
  getRestaurantSuccess,
  getRestaurantsSuccess,
} from './action';

function* fetchRestaurants() {
  const { offset } = yield select((state) => state.restaurantsReducer);

  try {
    yield put(isLoading(true));

    const response = yield call(api.get, `/restaurants?offset=${offset}`);
    const { data } = response;

    yield put(getRestaurantsSuccess(data));
  } catch (e) {
    yield put({ type: 'RESTAURANTS_FETCH_FAILED', message: e.message });
  } finally {
    yield put(isLoading(false));
  }
}

function* fetchPageRestaurants() {
  const { offset, limit } = yield select((state) => state.restaurantsReducer);

  if (offset < limit) {
    try {
      yield put(isLoading(true));

      const response = yield call(api.get, `/restaurants?offset=${offset}`);
      const { data } = response;

      yield put(getRestaurantsSuccess(data));
    } catch (e) {
      yield put({ type: 'RESTAURANTS_FETCH_FAILED', message: e.message });
    } finally {
      yield put(isLoading(false));
    }
  }
}

function* fetchRestaurant({ id }) {
  try {
    yield put(isLoading(true));

    const { data } = yield call(api.get, `/restaurants/${id}`);

    yield put(getRestaurantSuccess(data));
  } catch (e) {
    yield put({ type: 'RESTAURANTS_FETCH_FAILED', message: e.message });
  } finally {
    yield put(isLoading(false));
  }
}

function* restaurantsSaga() {
  yield takeLatest(GET_RESTAURANTS, fetchRestaurants);
  yield takeLatest(GET_RESTAURANT, fetchRestaurant);
  yield takeLatest(LOAD_RESTAURANTS, fetchPageRestaurants);
}

export default restaurantsSaga;