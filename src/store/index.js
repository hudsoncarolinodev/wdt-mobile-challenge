import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';

import restaurantsReducer from './restaurants/reducers';
import restaurantsSaga from './restaurants/saga';

import favoriteReducer from './favorites/reducers';
import favoriteSaga from './favorites/saga';

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  restaurantsReducer,
  favoriteReducer
});

const middlewares = applyMiddleware(thunk, sagaMiddleware);

const store = createStore(rootReducer, middlewares);

sagaMiddleware.run(restaurantsSaga);
sagaMiddleware.run(favoriteSaga);

export default store;