export const ADD_FAVORITE = 'ADD_FAVORITE';
export const SAVE_FAVORITE = 'SAVE_FAVORITE';
export const GET_FAVORITE = 'GET_FAVORITE';
export const SET_FAVORITE = 'SET_FAVORITE';

export const saveFavorite = (newRestaurantsFavorites) => ({
  type: SAVE_FAVORITE,
  payload: newRestaurantsFavorites,
});

export const addFavorite = (itemId) => ({
  type: ADD_FAVORITE,
  payload: {
    itemId: itemId,
  },
});

export const getFavorite = (favorite) => ({
  type: GET_FAVORITE,
  payload: favorite,
});

export const setStoreFavorite = (favorite) => ({
  type: SET_FAVORITE,
  payload: favorite,
});