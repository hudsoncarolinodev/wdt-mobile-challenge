import { call, put, select, takeLatest } from 'redux-saga/effects';
import  storage  from '../../services/asyncStorage';

import {
  ADD_FAVORITE,
  SAVE_FAVORITE,
  GET_FAVORITE,
  SET_FAVORITE
} from './action';

const KEY_ASYNC_STORAGE = '@restaurants:favorites';

function* addFavorite({ payload }) {
  const { itemId } = payload;
  
  const store = yield select();
  const { restaurantsFavorites } = store.favoriteReducer;

  let newRestaurantsFavorites;

  if (!restaurantsFavorites.includes(itemId)) {
    newRestaurantsFavorites = [...restaurantsFavorites, itemId];
  } else {
    newRestaurantsFavorites = restaurantsFavorites.filter(id => id !== itemId);
  }

  yield put({ type: SAVE_FAVORITE, payload: newRestaurantsFavorites });
}

function* saveFavorite() {
  const store = yield select();
  const { restaurantsFavorites } = store.favoriteReducer;

  try {
    yield call(storage.set, KEY_ASYNC_STORAGE, restaurantsFavorites);
  } catch (e) {
    yield put({ type: 'RESTAURANTS_FETCH_FAILED', message: e.message });
  }
}

function* fetchFavorites() {
  try {
    const data = yield call(storage.get, KEY_ASYNC_STORAGE);

    if (data.length) {
      yield put({ type: SET_FAVORITE, payload: data });
    }
  } catch (e) {
    yield put({ type: 'RESTAURANTS_FETCH_FAILED', message: e.message });
  }
}

function* favoriteSaga() {
  yield takeLatest(ADD_FAVORITE, addFavorite);
  yield takeLatest(SAVE_FAVORITE, saveFavorite);
  yield takeLatest(GET_FAVORITE, fetchFavorites);
}

export default favoriteSaga;