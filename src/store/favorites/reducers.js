import {
  ADD_FAVORITE,
  GET_FAVORITE,
  SAVE_FAVORITE,
  SET_FAVORITE
} from './action.js';

const initialState = {
  restaurantsFavorites: [],
};

const favoriteReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FAVORITE:
      return state;
    case SAVE_FAVORITE:
      return {
        ...state,
        restaurantsFavorites: [...action.payload]
      };
    case GET_FAVORITE:
      return state;
    case SET_FAVORITE:
      return {
        ...state,
        restaurantsFavorites: [...action.payload]
      };
    default:
      return state;
  }
};

export default favoriteReducer;