import * as React from 'react';
import {View} from 'react-native';
import { theme } from '../../theme';

export default function Container({children}) {
  return <View style={theme.Container}>{children}</View>
}