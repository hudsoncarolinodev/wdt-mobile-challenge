import * as React from 'react';
import {Text} from 'react-native';
import { theme } from './../../theme';

export default function Heading({as, children, style}) {
  return <Text  style={{...theme.Heading[as], ...style}}>{children}</Text>
}