import * as React from 'react';
import {FlatList, Image} from 'react-native';
import ListItem from './components/ListItem';
import { useSelector, useDispatch } from 'react-redux'
import { loadPageRestaurant } from './../../store/restaurants/action';
import IconLoad from './../../components/IconLoad'

export default function RestaurantsListComponent(){

  let dispatch = useDispatch()
  const { restaurants, loading } = useSelector((state) => state.restaurantsReducer);
  
  React.useEffect(()=>{
 
  },[restaurants])

  const loadRestaurants = () =>{
    dispatch(loadPageRestaurant())
  }

  return (<FlatList 
    data={restaurants}
    renderItem={({item}) => 
      <ListItem 
        image={item.image?.url}
        name={item.name} 
        address={item.addressInfo?.address}
        id={item._id}
      />
    }
    onEndReached={loadRestaurants}
    onEndReachedThreshold={0.1}
    ListFooterComponent={()=> <IconLoad state={loading} />}
    keyExtractor={(item, index) => `${index}_${item._id}`}
  />)
}
