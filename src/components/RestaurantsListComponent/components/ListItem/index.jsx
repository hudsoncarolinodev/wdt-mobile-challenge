import * as React from 'react';
import {View, Image, TouchableNativeFeedback} from 'react-native';
import FavoriteButton from './../../../FavoriteButton'
import Heading from '../../../Heading/index';
import { theme } from '../../../../theme';
import {styleItem} from './style'
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux'

export default function ListItem({image, name, address, id}) {
  
  const { restaurantsFavorites } = useSelector((state) => state.favoriteReducer);

  const navigation = useNavigation();

  const isFavorite = restaurantsFavorites.some((itemId)=> itemId == id)

  React.useEffect(()=>{

  },[restaurantsFavorites])
 
  const handlePress = () => {
    navigation.navigate('Detalhes do restaurante', {
      itemId:id
    })
  }

  return (
    <TouchableNativeFeedback 
      onPress={handlePress}
      background={TouchableNativeFeedback.Ripple(
        theme.Color.lightGray,
        false,
      )}
      >
      <View style={styleItem.item}>
        <View style={styleItem.description}>
          <Image
            source={
              image?{uri:image}:
              require('./../../../../../assets/images/placeholder.jpg')
            }
            style={styleItem.image}
          />
        </View>
        <View style={styleItem.description}>
          <Heading 
            style={styleItem.title}
            as="h5"
          >{name}</Heading>
          <Heading 
            style={styleItem.title}
            as="p2"
          >{address}</Heading>
        </View>

        <View style={styleItem.icon}>
          <FavoriteButton  initialStateButton={isFavorite} itemId={id}/>
        </View>
      </View>
    </TouchableNativeFeedback>
  )
}