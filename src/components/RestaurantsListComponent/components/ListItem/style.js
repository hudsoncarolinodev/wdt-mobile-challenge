export const styleItem = {

    item:{
        flexDirection:"row",
        marginBottom:20,
        alignItems:"center",
        gap:10,
        paddingTop:5,
        paddingBottom:5,
        position:"relative",
    },
    image:{
        width:70,
        height:70,
        borderRadius:50,
    },
    title:{
        width:190,
    },
    icon:{
        alignSelf:"center",
        position:"absolute",
        right:5
    }

}