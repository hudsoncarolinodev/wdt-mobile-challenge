import * as React from 'react';
import {Image, View} from 'react-native';
import Heading from './../../../Heading';
import {style} from './style'
import { theme } from './../../../../theme';
import FavoriteButton from './../../../FavoriteButton'
import { useSelector } from 'react-redux'

export default function Header({id, image, name, addressInfo, contacts}) {
  const { restaurantsFavorites } = useSelector((state) => state.favoriteReducer)
  const isFavoriteList = restaurantsFavorites.some((itemId)=> itemId == id)
  
  return (
    <View style={style.page}>

      <Image
          source={
            image?{uri:image}:
            require('./../../../../../assets/images/placeholder.jpg')
          }
          style={style.image}
        />

      <View  style={theme.Container}>
        <View style={style.section}>
         
          <View style={style.header}> 
            <Heading as="h2">{name} - {addressInfo?.country} </Heading>
            <FavoriteButton  initialStateButton={isFavoriteList} itemId={id}/>
          </View>
          
          <Heading as="p2">{addressInfo?.address}, {addressInfo?.city} - {addressInfo?.postalCode}</Heading>
         
        </View>

        <View style={style.section}>
          <Heading as="h4">Contatos:</Heading>
          <Heading as="p2">E-mail: {contacts?.email}</Heading>
          <Heading as="p2">Phone:  {contacts?.phoneNumber}</Heading>
        </View>
      </View>

    </View>
  )
}

