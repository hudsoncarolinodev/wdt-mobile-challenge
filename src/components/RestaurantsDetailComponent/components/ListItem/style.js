export const styleItem = {

    item:{
        flexDirection:"row",
        marginBottom:20,
        alignItems:"center",
        gap:20,
        paddingTop:5,
        paddingBottom:10,
        borderBottomWidth:1,
        borderBottomColor:"#EDF2F7",
    },
    image:{
        width:70,
        height:70,
        borderRadius:50
    },
    title:{},
    description:{}

}