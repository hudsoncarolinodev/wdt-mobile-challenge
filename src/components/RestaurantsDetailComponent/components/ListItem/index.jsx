import * as React from 'react';
import {View} from 'react-native';
import Heading from '../../../Heading/index';
import {styleItem} from './style'

export default function ListItem({name, address}) {
  
  return (
    <View style={styleItem.item}>
      <View>
        <Heading 
          as="h5"
        >{name}</Heading>
        <Heading 
          as="p"
        >{address}</Heading>
      </View>
    </View>
  )
}