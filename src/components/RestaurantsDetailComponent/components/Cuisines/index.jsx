import * as React from 'react';
import {FlatList, View} from 'react-native';
import Heading from '../../../../components/Heading';
import {style} from './style'
import { theme } from './../../../../theme';
import ListItem from './../ListItem'
export default function Cuisines({cuisines}) {

  return (
    <View style={style.page}>
        <View style={theme.Container}>
            <Heading as="h2">Cozinhas:</Heading>

            <FlatList 
                data={cuisines}
                renderItem={({item}) => 
                <ListItem 
                    name={item.name['pt-BR']} 
                    address={item.tag}
                    id={item._id}
                />
                }
                keyExtractor={item => item._id}
            />
        </View>
    </View>
  )
}

