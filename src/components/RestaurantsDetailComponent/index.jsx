import * as React from 'react';
import {View} from 'react-native';
import {style} from './style'
import Header from './components/Header'
import Cuisines from './components/Cuisines'

export default function RestaurantsDetailComponent({restaurant}) {
  const {image, name, addressInfo, contacts, cuisines} = restaurant

  return (
    <View style={style.page}>

      <Header 
        name={name}
        image={image?.url}
        addressInfo={addressInfo}
        contacts={contacts}
        id={restaurant._id}
      />

      <Cuisines cuisines={cuisines} />
      
    </View>
  )
}

