import * as React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import { useSelector} from 'react-redux'

export default function IconLoad({state}) {

  const [IconState, setIconState] = React.useState(false);
  
  React.useEffect(()=>{
    const newState = state
    setIconState(newState)
  },[state])

  if(IconState){
    return (
        <View style={styles.container}>
          <Image
            style={styles.img}
            source={
              require('./../../../assets/images/load.gif')
            }
          />
        </View>
    )
  }
  return <View></View>
}

const styles = StyleSheet.create({
  container:{
    justifyContent:"center",
    alignItems:"center",
    padding:10,
  },
  img:{
    width:30,
    height:30,
  }
});
