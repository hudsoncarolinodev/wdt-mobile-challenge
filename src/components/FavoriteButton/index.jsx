import * as React from 'react';
import {View, TouchableOpacity} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux'
import { addFavorite } from './../../store/favorites/action'

export default function FavoriteButton({initialStateButton, itemId}) {

  let dispatch = useDispatch()
  const [buttonState, setButtonState] = React.useState(initialStateButton);
 
  React. useEffect(() => {
    setButtonState(initialStateButton);
  }, [initialStateButton]);

  const handleClick = () => {
    const newButtonState = !buttonState;
    setButtonState(newButtonState);
    dispatch(addFavorite(itemId))
  };
  
  return (
    <TouchableOpacity onPress={handleClick}>
      <View>
        <Ionicons 
          name={buttonState? "md-heart":"md-heart-outline"}
          size={30} 
          color="#d22e2e" 
        />
      </View>
    </TouchableOpacity>
  )
}