
import AsyncStorage from '@react-native-async-storage/async-storage';
//AsyncStorage.clear()
const storage = {
    async set(key, data) {
      try {
        await saveData(key, data);
      } catch (e) {
        console.log(e);
      }
    },
  
    async get(key) {
      
      const data = await getData(key);
      if (data !== null) {
        return JSON.parse(data);
      }
    },
  };
  
async function saveData(key, data) {
    await AsyncStorage.setItem(key, JSON.stringify(data));
}
  
async function getData(key) {
    return await AsyncStorage.getItem(key);
}
  
export default storage;