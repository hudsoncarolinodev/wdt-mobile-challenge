import axios from 'axios'
const url = 'https://api.dev.wdtek.xyz'

const axiosInstance = axios.create({ baseURL: url })

axiosInstance.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

axiosInstance.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });

export const api = {
    get(endpoint) {
      return axiosInstance.get(endpoint)
    },
}
  