import * as React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import RestaurantsDetailComponent from './../../../components/RestaurantsDetailComponent'
import { getRestaurant } from './../../../store/restaurants/action';
import IconLoad from '../../../components/IconLoad/index';
export default function RestaurantsDetail({route}) {
  
  let dispatch = useDispatch()
  const { restaurant, loading } = useSelector((state) => state.restaurantsReducer);
  const { itemId } = route.params

  React.useEffect(()=>{
    dispatch(getRestaurant(itemId))
  },[])

  if(loading) return <IconLoad state={loading}/>
  
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <RestaurantsDetailComponent 
        restaurant={restaurant}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
