import * as React from 'react';
import { StatusBar } from 'expo-status-bar';
import RestaurantsListComponent from './../../../components/RestaurantsListComponent'
import Container from './../../../components/Container'
import { getRestaurants } from './../../../store/restaurants/action';
import { useSelector, useDispatch } from 'react-redux'

export default function RestaurantsList({ navigation }) {
 let dispatch = useDispatch()

 React.useEffect(()=>{
   dispatch(getRestaurants())
   dispatch({ type: 'GET_FAVORITE' });
  },[])

  return (
    <Container>
        <StatusBar style="auto" />
       
        <RestaurantsListComponent/>
     
    </Container>
  );
}


